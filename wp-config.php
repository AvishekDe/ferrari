<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ferrari');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysterytriangle');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8c}(?O;*ezD9}WA~?90sm]lvx#%uQ]GA?LY1mxz-<W`<9igT#.y|wu6Qf+/1xBcO');
define('SECURE_AUTH_KEY',  '0Am>E_wiuD~?R}&-BQCq!@XMU,w~3/t [hkx!skB*;d|@FTh*GB*|`j=cqa^/iwu');
define('LOGGED_IN_KEY',    ']CsT(H<r{>]WGbp^tE;#-y,-|AEiu7xO.n~*Oj0x{f,}r|ss+XCI)7utfhY81)eM');
define('NONCE_KEY',        '^y-qU15l!!|A05j-Jjip$W01P<fXp6Gnl9jr*%d9sk$?tAnB$Cc0n!NslD~Rv/0M');
define('AUTH_SALT',        '92p`W+*`0&/V6PlZXg-Pq|A|g?->3M{8Owbf{`BwAJO|M&HJYU_LZizY5eCn7cK1');
define('SECURE_AUTH_SALT', 'CN:o^=/`zqLT}zM6LFER0pZQ-4.C{|EugR$4KWR@x](Wm_tH0M=+:l+|_?@_<lv7');
define('LOGGED_IN_SALT',   'oI{%`/sr->1{2lB%}aqk J_06sz+H[ON(]fv(~fI42O##OJ64YzIQEw3-ACMQT(|');
define('NONCE_SALT',       '7$6-8VbqiW#j}+I.UC^QJ;tv.GZD4Us|k7cW}tiZdH|hVM*-t|9k+qOg+K##lQ;|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

